# IndiBlog
is a Blog Website we created using VueJS for our final project in Jabar Coding Camp

## Member List
- Chrisanta Veronica @chrisantavt
- Rafli Surya Pratama @RSurya99
- Usman lubis @usmanlubis

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
