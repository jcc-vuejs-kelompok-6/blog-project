import Vue from 'vue'
import Vuex from 'vuex'
import alert from './alert'
import dialog from './dialog'
import dialogblog from './dialogblog'
import auth from './auth'
import update from './update'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
    key: 'sanbercode',
    storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules: {
        alert,
        dialog,
        dialogblog,
        auth,
        update,
    }
})