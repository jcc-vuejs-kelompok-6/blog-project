export default {
    namespaced: true,
    state: {
        id: null,
        title: '',
        description: '',
        refresh: false
    },
    mutations: {
        setData: (state, payload) => {
            state.id = payload.id
            state.title = payload.title
            state.description = payload.description
        },
        updateTitle: (state, payload) => {
            state.title = payload
        },
        updateDesc: (state, payload) => {
            state.description = payload
        },
        updateRefresh: (state, payload) => {
            state.refresh = payload
        },
        clear: (state)=>{
            state.id = null,
            state.title = '',
            state.description = ''
            state.refresh = true
        },
    },
    actions: {
        setData: ({commit}, payload) => {
            commit('setData', payload)
        },
        updateRefresh: ({commit}, payload) => {
            commit('updateRefresh', payload)
        },
        clear: ({commit})=>{
            commit('clear')
        },
    },
    getters: {
        id: state => state.id,
        title: state => state.title,
        description: state => state.description,
        refresh: state => state.refresh,
    }
}